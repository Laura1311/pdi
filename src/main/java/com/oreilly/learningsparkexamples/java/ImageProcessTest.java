package com.oreilly.learningsparkexamples.java;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.ArrayUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.util.Progressable;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.sql.catalyst.expressions.In;
import scala.Int;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

public class ImageProcessTest {
	BufferedImage image;
	int width;
	int height;

	public ImageProcessTest() {

		try {
			File input = new File("/home/centos/test_image.jpg");
			image = ImageIO.read(input);
			width = image.getWidth();
			height = image.getHeight();

			for(int i=0; i<height; i++){

				for(int j=0; j<width; j++){

					Color c = new Color(image.getRGB(j, i));
					int red = (int)(c.getRed() * 0.299);
					int green = (int)(c.getGreen() * 0.587);
					int blue = (int)(c.getBlue() *0.114);
					Color newColor = new Color(red+green+blue,

							red+green+blue,red+green+blue);

					image.setRGB(j,i,newColor.getRGB());
				}
			}

			File ouptut = new File("grayscale.jpg");
			ImageIO.write(image, "jpg", ouptut);

		} catch (Exception e) {}
	}

	public static void loadImage(String hdfsRootPath, String hdfsFolderPath, byte[] img, Integer index) throws IOException, URISyntaxException {
		//1. Get the instance of COnfiguration
		Configuration configuration = new Configuration();
		//2. Create an InputStream to read the data from local filejava fi
		//InputStream inputStream = new FileInputStream(imgFile);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(img);
		oos.flush();
		oos.close();
		InputStream inputStream = new ByteArrayInputStream(baos.toByteArray());

		//3. Get the HDFS instance
		FileSystem hdfs = FileSystem.get(new URI(hdfsRootPath), configuration);
		//4. Open a OutputStream to write the data, this can be obtained from the FileSytem
		OutputStream outputStream = hdfs.create(new Path(hdfsFolderPath + index + ".jpg"),
				new Progressable() {
					@Override
					public void progress() {
						//System.out.println("....");
					}
				});
		try
		{
			IOUtils.copyBytes(inputStream, outputStream, 4096, false);
		}
		finally
		{
			IOUtils.closeStream(inputStream);
			IOUtils.closeStream(outputStream);
		}

	}
	public static ConvolveOp getGaussianBlurFilter(int radius,
												   boolean horizontal) {
		if (radius < 1) {
			throw new IllegalArgumentException("Radius must be >= 1");
		}

		int size = radius * 2 + 1;
		float[] data = new float[size];

		float sigma = radius / 3.0f;
		float twoSigmaSquare = 2.0f * sigma * sigma;
		float sigmaRoot = (float) Math.sqrt(twoSigmaSquare * Math.PI);
		float total = 0.0f;

		for (int i = -radius; i <= radius; i++) {
			float distance = i * i;
			int index = i + radius;
			data[index] = (float) Math.exp(-distance / twoSigmaSquare) / sigmaRoot;
			total += data[index];
		}

		for (int i = 0; i < data.length; i++) {
			data[i] /= total;
		}

		Kernel kernel = null;
		if (horizontal) {
			kernel = new Kernel(size, 1, data);
		} else {
			kernel = new Kernel(1, size, data);
		}
		return new ConvolveOp(kernel, ConvolveOp.EDGE_NO_OP, null);
	}

	public static Kernel makeKernel(float radius) {
		int r = (int)Math.ceil(radius);
		int rows = r*2+1;
		float[] matrix = new float[rows];
		float sigma = radius/3;
		float sigma22 = 2*sigma*sigma;
		float sigmaPi2 = 2*(float)Math.PI*sigma;
		float sqrtSigmaPi2 = (float)Math.sqrt(sigmaPi2);
		float radius2 = radius*radius;
		float total = 0;
		int index = 0;
		for (int row = -r; row <= r; row++) {
			float distance = row*row;
			if (distance > radius2)
				matrix[index] = 0;
			else
				matrix[index] = (float)Math.exp(-(distance)/sigma22) / sqrtSigmaPi2;
			total += matrix[index];
			index++;
		}
		for (int i = 0; i < rows; i++)
			matrix[i] /= total;

		return new Kernel(rows, 1, matrix);
	}

	public static void main(String[] args) throws Exception {
		SparkConf sparkConf = new SparkConf().setAppName("JavaSparkGrayscale");
		JavaSparkContext javaSparkContext = new JavaSparkContext(sparkConf);

		String hdfsRootPath = "/home/centos/hadoop_dir/";

		Path path = new Path(args[0]);
		FileSystem fileSystem = FileSystem.get(new URI(hdfsRootPath), new Configuration());
		FSDataInputStream input = fileSystem.open(path);
		BufferedImage inputImage = ImageIO.read(input);

		int width = inputImage.getWidth();
		int height = inputImage.getHeight();

		int[] pixels = inputImage.getRGB(0, 0, width, height, null, 0, width);

		//JavaRDD<Integer>  img = javaSparkContext.parallelize(Arrays.asList(ArrayUtils.toObject(pixels)), Integer.valueOf(args[4]));
		Broadcast<int[]> br = javaSparkContext.broadcast(pixels);

		JavaRDD<Integer>  img = javaSparkContext.parallelize(Arrays.asList(ArrayUtils.toObject(br.value())), Integer.valueOf(args[4]));

		Function<Integer, Integer> negativeFilter = (i) -> {
			int red = (i >> 16) & 0xff;
			int green = (i >> 8) & 0xff;
			int blue = i & 0xff;
			return ((red << 16) & 0xff) + ((green << 8) & 0xff) + (blue & 0xff);
		};

		Function<Integer, Integer> gray = (i) -> {
			int red = (i >> 16) & 0xff;
			int green = (i >> 8) & 0xff;
			int blue = i & 0xff;
			red *= 0.299;
			green *= 0.587;
			blue *= 0.114;

			int res = red+green+blue;
			i = (res<<16) | (res<<8) | res;
			return i;
		};

		JavaRDD<Integer> result;
		if (args[2].equals("GRAY")) {
			result = img.map(gray);
		} else {
			result=img.map(negativeFilter);
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////////
//		Kernel kernel = makeKernel(2);

		int[] filter = {1, 2, 1, 2, 4, 2, 1, 2, 1};
		int radius = 2;
//		ConvolveOp convolveOp = getGaussianBlurFilter(radius / 2, true);
//		float[] matrixKernel = new float[];
//		convolveOp.getKernel().getKernelData(matrixKernel);

		int filterWidth = 3;
		int pixelIndexOffset = width - filterWidth;
		int centerOffsetX = filterWidth / 2;
		int centerOffsetY = filter.length / filterWidth / 2;
		double sum = IntStream.of(filter).sum();

		Function<Integer, Integer> gaussianBlur = (in) -> {
			int h = height - filter.length / filterWidth + 1, w = width - filterWidth + 1, y = 0;
			int x = 0;
			int r = 0;
			int g = 0;
			int b = 0;
			for (int filterIndex = 0, pixelIndex = y * width + x;
				 filterIndex < filter.length;
				 pixelIndex += pixelIndexOffset) {
				for (int fx = 0; fx < filterWidth; fx++, pixelIndex++, filterIndex++) {
					int factor = filter[filterIndex];

					// sum up color channels seperately
					r += ((in >>> 16) & 0xFF) * factor;
					g += ((in >>> 8) & 0xFF) * factor;
					b += (in & 0xFF) * factor;
				}
			}
			r /= sum;
			g /= sum;
			b /= sum;
			// combine channels with full opacity
//				output[x + centerOffsetX + (y + centerOffsetY) * width] = (r << 16) | (g << 8) | b | 0xFF000000;

			return ((r << 16) | (g << 8) | b | 0xFF000000);
		};


//		result = img.map(gaussianBlur);

////////////////////////////////////////////////////////////////////////////////////////////////////////

		List<Integer> outputPixels = result.collect();

		if (Arrays.asList(ArrayUtils.toObject(pixels)).equals(outputPixels)) {
			System.out.println("THEY ARE EQUAL");
		} else {
			System.out.println("THEY ARE NOOOOOOOOOOOOT EQUAL");
		}

//		System.arraycopy(ArrayUtils.toPrimitive(outputPixels.toArray(new Integer[outputPixels.size()])), 0, pixels, 0, pixels.length);
		inputImage.setRGB(0, 0, width, height, ArrayUtils.toPrimitive(outputPixels.toArray(new Integer[outputPixels.size()])), 0, width);

		//Write image to hdfs
		Configuration configuration = new Configuration();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(inputImage, "jpg", baos);
		baos.flush();
		byte[] imgBytes = baos.toByteArray();


		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(imgBytes);

		oos.flush();
		oos.close();
		InputStream inputStream = new ByteArrayInputStream(baos.toByteArray());

		//3. Get the HDFS instance
		FileSystem hdfs = FileSystem.get(new URI(hdfsRootPath), configuration);
		//4. Open a OutputStream to write the data, this can be obtained from the FileSytem
		OutputStream outputStream = hdfs.create(new Path(hdfsRootPath + args[1]),
				new Progressable() {
					@Override
					public void progress() {
						//System.out.println("....");
					}
				});
		try
		{
			IOUtils.copyBytes(inputStream, outputStream, 4096, false);
		}
		finally
		{
			IOUtils.closeStream(inputStream);
			IOUtils.closeStream(outputStream);
		}

		javaSparkContext.stop();
		System.out.println("FINAL");
	}

}
