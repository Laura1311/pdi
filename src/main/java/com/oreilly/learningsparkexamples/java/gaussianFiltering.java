package com.oreilly.learningsparkexamples.java;

import java.awt.*;
import java.awt.image.*;
import java.applet.*;
import java.net.*;
import java.io.*;
import java.lang.Math;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.JApplet;
import javax.imageio.*;
import javax.swing.event.*;

public class gaussianFiltering extends JApplet {

    Image edgeImage, accImage, outputImage;
    MediaTracker tracker = null;
    PixelGrabber grabber = null;
    int width = 0, height = 0;
    String fileNames[] = {"lena.png", "microphone.png", "screw.png", "drawing.png", "film.png"};

    javax.swing.Timer timer;
    //slider constraints
    static final int SIGMA_MIN = 1;
    static final int SIGMA_MAX = 16;
    static final int SIGMA_INIT = 1;
    int sigma=SIGMA_INIT;
    static final int RADIUS_MIN = 5;
    static final int RADIUS_MAX = 45;
    static final int RADIUS_INIT = 7;
    int radius=RADIUS_INIT;
    static final int NOISE_MIN = 0;
    static final int NOISE_MAX = 50;
    static final int NOISE_INIT = 5;
    static int noise=NOISE_INIT;

    int imageNumber=0;
    static int progress=0;
    public int orig[] = null;

    Image image[] = new Image[fileNames.length];

    JProgressBar progressBar;
    JPanel controlPanel,noisePanel, imagePanel, progressPanel;
    JLabel origLabel, outputLabel,noiseLabel,noiseLabel2,noiseLabel3,comboLabel,sigmaLabel,radiusLabel,processing;
    JSlider sigmaSlider, radiusSlider, noiseSlider;
    JRadioButton gaussianRadio, condimentRadio;
    JComboBox imSel;
    static PDIGaussian filter;
    static gaussianNoise gNoise;
    static condimentNoise cNoise;
    ButtonGroup radiogroup;
    String noisemode="Gaussian";


    // Applet init function
    public void init() {

        tracker = new MediaTracker(this);
        for(int i = 0; i < fileNames.length; i++) {
            image[i] = getImage(this.getCodeBase(),fileNames[i]);
            image[i] = image[i].getScaledInstance(256, 256, Image.SCALE_SMOOTH);
            tracker.addImage(image[i], i);
        }
        try {
            tracker.waitForAll();
        }
        catch(InterruptedException e) {
            System.out.println("error: " + e);
        }

        Container cont = getContentPane();
        cont.removeAll();
        cont.setBackground(Color.black);
        cont.setLayout(new BorderLayout());

        controlPanel = new JPanel();
        controlPanel.setLayout(new GridLayout(2,5,15,0));
        controlPanel.setBackground(new Color(192,204,226));
        imagePanel = new JPanel();
        imagePanel.setBackground(new Color(192,204,226));
        progressPanel = new JPanel();
        progressPanel.setBackground(new Color(192,204,226));
        progressPanel.setLayout(new GridLayout(2,1));
        noisePanel = new JPanel();
        noisePanel.setBackground(new Color(192,204,226));
        noisePanel.setLayout(new GridLayout(2,1));

        comboLabel = new JLabel("IMAGE");
        comboLabel.setHorizontalAlignment(JLabel.CENTER);
        controlPanel.add(comboLabel);

        noiseLabel3 = new JLabel("NOISE TYPE");
        noiseLabel3.setHorizontalAlignment(JLabel.CENTER);
        controlPanel.add(noiseLabel3);

        noiseLabel = new JLabel("NOISE = 5%");
        noiseLabel.setHorizontalAlignment(JLabel.CENTER);
        controlPanel.add(noiseLabel);



        sigmaLabel = new JLabel("SIGMA = "+SIGMA_INIT);
        sigmaLabel.setHorizontalAlignment(JLabel.CENTER);
        controlPanel.add(sigmaLabel);
        radiusLabel = new JLabel("RADIUS = "+RADIUS_INIT);
        radiusLabel.setHorizontalAlignment(JLabel.CENTER);
        controlPanel.add(radiusLabel);


        processing = new JLabel("Processing...");
        processing.setHorizontalAlignment(JLabel.LEFT);
        progressBar = new JProgressBar(0,100);
        progressBar.setValue(0);
        progressBar.setStringPainted(true); //get space for the string
        progressBar.setString("");          //but don't paint it
        progressPanel.add(processing);
        progressPanel.add(progressBar);

        width = image[imageNumber].getWidth(null);
        height = image[imageNumber].getHeight(null);

        imSel = new JComboBox(fileNames);
        imageNumber = imSel.getSelectedIndex();
        imSel.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        imageNumber = imSel.getSelectedIndex();
                        origLabel.setIcon(new ImageIcon(image[imageNumber]));
                        processImage();
                    }
                }
        );
        controlPanel.add(imSel, BorderLayout.PAGE_START);

        timer = new javax.swing.Timer(10, new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                progressBar.setValue(filter.getProgress());
            }
        });

        origLabel = new JLabel("Original Image", new ImageIcon(image[imageNumber]), JLabel.CENTER);
        origLabel.setVerticalTextPosition(JLabel.BOTTOM);
        origLabel.setHorizontalTextPosition(JLabel.CENTER);
        origLabel.setForeground(Color.blue);
        imagePanel.add(origLabel);

        noiseLabel2 = new JLabel("Noisy Image", new ImageIcon(image[imageNumber]), JLabel.CENTER);
        noiseLabel2.setVerticalTextPosition(JLabel.BOTTOM);
        noiseLabel2.setHorizontalTextPosition(JLabel.CENTER);
        noiseLabel2.setForeground(Color.blue);
        imagePanel.add(noiseLabel2);

        outputLabel = new JLabel("Gaussian Filtered", new ImageIcon(image[imageNumber]), JLabel.CENTER);
        outputLabel.setVerticalTextPosition(JLabel.BOTTOM);
        outputLabel.setHorizontalTextPosition(JLabel.CENTER);
        outputLabel.setForeground(Color.blue);
        imagePanel.add(outputLabel);


        gaussianRadio = new JRadioButton("Gaussian");
        gaussianRadio.setActionCommand("Gaussian");
        gaussianRadio.setBackground(new Color(192,204,226));
        gaussianRadio.setHorizontalAlignment(SwingConstants.CENTER);
        condimentRadio = new JRadioButton("Condiment");
        condimentRadio.setActionCommand("Condiment");
        condimentRadio.setHorizontalAlignment(SwingConstants.CENTER);
        condimentRadio.setBackground(new Color(192,204,226));
        gaussianRadio.setSelected(true);
        radiogroup = new ButtonGroup();
        radiogroup.add(condimentRadio);
        radiogroup.add(gaussianRadio);
        condimentRadio.addActionListener(new radiolistener());
        gaussianRadio.addActionListener(new radiolistener());
        noisePanel.add(gaussianRadio);
        noisePanel.add(condimentRadio);
        controlPanel.add(noisePanel);

        noiseSlider = new JSlider(JSlider.HORIZONTAL, NOISE_MIN, NOISE_MAX, NOISE_INIT);
        noiseSlider.addChangeListener(new noiseListener());
        noiseSlider.setMajorTickSpacing(10);
        noiseSlider.setMinorTickSpacing(2);
        noiseSlider.setPaintTicks(true);
        noiseSlider.setPaintLabels(true);
        noiseSlider.setBackground(new Color(192,204,226));
        controlPanel.add(noiseSlider);


        sigmaSlider = new JSlider(JSlider.HORIZONTAL, SIGMA_MIN, SIGMA_MAX, SIGMA_INIT);
        sigmaSlider.addChangeListener(new sigmaListener());
        sigmaSlider.setMajorTickSpacing(5);
        sigmaSlider.setMinorTickSpacing(1);
        sigmaSlider.setPaintTicks(true);
        sigmaSlider.setPaintLabels(true);
        sigmaSlider.setBackground(new Color(192,204,226));
        controlPanel.add(sigmaSlider);

        radiusSlider = new JSlider(JSlider.HORIZONTAL, RADIUS_MIN, RADIUS_MAX, RADIUS_INIT);
        radiusSlider.addChangeListener(new radiusListener());
        radiusSlider.setMajorTickSpacing(10);
        radiusSlider.setMinorTickSpacing(2);
        radiusSlider.setPaintTicks(true);
        radiusSlider.setPaintLabels(true);
        radiusSlider.setBackground(new Color(192,204,226));
        controlPanel.add(radiusSlider);

        cont.add(controlPanel, BorderLayout.NORTH);
        cont.add(imagePanel, BorderLayout.CENTER);
        cont.add(progressPanel, BorderLayout.SOUTH);

        processImage();

    }
    class radiolistener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            noisemode=e.getActionCommand();
            processImage();
        }
    }
    class sigmaListener implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            JSlider source = (JSlider)e.getSource();
            if (!source.getValueIsAdjusting()) {
                System.out.println("sigma="+source.getValue());
                sigma=source.getValue();
                sigmaLabel.setText("SIGMA = "+source.getValue());
                processImage();
            }
        }
    }
    class radiusListener implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            JSlider source = (JSlider)e.getSource();
            if (!source.getValueIsAdjusting()) {
                System.out.println("radius="+source.getValue());
                radius=source.getValue();
                radiusLabel.setText("RADIUS = "+source.getValue());
                processImage();
            }
        }
    }
    class noiseListener implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            JSlider source = (JSlider)e.getSource();
            if (!source.getValueIsAdjusting()) {
                System.out.println("noise="+source.getValue()+"%");
                noise=source.getValue();
                noiseLabel.setText("NOISE = "+source.getValue()+"%");
                processImage();
            }
        }
    }
    private void processImage(){
        orig=new int[width*height];
        PixelGrabber grabber = new PixelGrabber(image[imageNumber], 0, 0, width, height, orig, 0, width);
        try {
            grabber.grabPixels();
        }
        catch(InterruptedException e2) {
            System.out.println("error: " + e2);
        }
        progressBar.setMaximum(width-radius);

        processing.setText("Processing...");
        sigmaSlider.setEnabled(false);
        radiusSlider.setEnabled(false);
        imSel.setEnabled(false);
        gaussianRadio.setEnabled(false);
        condimentRadio.setEnabled(false);
        noiseSlider.setEnabled(false);

        gNoise = new gaussianNoise();
        cNoise = new condimentNoise();
        filter = new PDIGaussian();

        gNoise.init(orig,width,height,(float)noise);
        cNoise.init(orig,width,height,(float)noise/100);
        if(noisemode=="Gaussian"){
            orig=gNoise.process();
        }
        if(noisemode=="Condiment"){
            orig=cNoise.process();
        }

        filter.init(orig,sigma,radius,width,height);
        filter.generateTemplate();

        timer.start();
        new Thread(){
            public void run(){
                final Image output = createImage(new MemoryImageSource(width, height, filter.process(), 0, width));
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        outputLabel.setIcon(new ImageIcon(output));
                        if(noisemode=="Gaussian"){
                            noiseLabel2.setIcon(new ImageIcon(createImage(new MemoryImageSource(width, height, gNoise.process(), 0, width))));
                        }
                        if(noisemode=="Condiment"){
                            noiseLabel2.setIcon(new ImageIcon(createImage(new MemoryImageSource(width, height, cNoise.process(), 0, width))));
                        }
                        processing.setText("Done");
                        sigmaSlider.setEnabled(true);
                        radiusSlider.setEnabled(true);
                        imSel.setEnabled(true);
                        gaussianRadio.setEnabled(true);
                        condimentRadio.setEnabled(true);
                        noiseSlider.setEnabled(true);
                    }
                });
            }
        }.start();
    }

}