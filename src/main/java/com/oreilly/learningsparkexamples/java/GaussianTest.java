package com.oreilly.learningsparkexamples.java;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;

/**
 * Created by laura on 5/7/17.
 */
public class GaussianTest {

    public static void main(String[] args) {

        File input = new File("/home/laura/Desktop/digital_image_processing.jpg");
        BufferedImage image = null;
        try {
            image = ImageIO.read(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int width = image.getWidth();
        int height = image.getHeight();

        int orig[] = new int[width*height];
        int sigma = 1;
        int radius = 7;

        PDIGaussian pdiGaussian = new PDIGaussian();
        pdiGaussian.init(orig,sigma,radius,width,height);
        pdiGaussian.generateTemplate();

//        int[] pixels = pdiGaussian.process();
        BufferedImage out = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        out.setRGB(0, 0, width, height, pdiGaussian.process(), 0, width);

        File outputFile = new File("grayscale.jpg");
        try {
            ImageIO.write(out, "jpg", outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
